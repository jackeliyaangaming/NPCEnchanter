package me.jackyftw.enchanter.utils;

import me.jackyftw.enchanter.NPCEnchanter;
import org.bukkit.ChatColor;

import java.util.TreeMap;
import java.util.logging.Level;

public class Utils {

    public static void log(Level level, String log) {
        NPCEnchanter.instance.getLogger().log(level, log);
    }

    public static String color(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();
    private final static TreeMap<String, Integer> map2 = new TreeMap<>();

    private static void compileMap() {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    }

    public static String toRoman(int number) {
        compileMap();
        int l =  map.floorKey(number);
        if(number == l) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number - l);
    }

    public static int romanToInt(String roman) {
        int ans = 0;
        for (int i = roman.length() - 1; i >= 0; i--) {
            char c = roman.charAt(i);
            switch (c) {
                case 'I':
                    ans += (ans >= 5 ? -1 : 1);
                    break;
                case 'V':
                    ans += 5;
                    break;
                case 'X':
                    ans += 10 * (ans >= 50 ? -1 : 1);
                    break;
                case 'L':
                    ans += 50;
                    break;
                case 'C':
                    ans += 100 * (ans >= 500 ? -1 : 1);
                    break;
                case 'D':
                    ans += 500;
                    break;
                case 'M':
                    ans += 1000;
                    break;
            }
        }
        return ans;
    }

    public static String capitalize(String string) {
        String[] letters = string.split("");
        letters[0] = letters[0].replace(letters[0], letters[0].toUpperCase());
        StringBuilder builder = new StringBuilder();

        for(String letter : letters) {
            builder.append(letter);
        }
        return builder.toString();
    }

}
