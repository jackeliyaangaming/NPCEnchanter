package me.jackyftw.enchanter.listeners;

import me.jackyftw.enchanter.NPCEnchanter;
import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.enchant.EnchantManager;
import static me.jackyftw.enchanter.utils.Utils.color;
import static me.jackyftw.enchanter.utils.Utils.romanToInt;

import me.jackyftw.enchanter.file.FileManager;
import me.jackyftw.enchanter.gui.ToolGUI;
import me.jackyftw.enchanter.gui.ToolGUIManager;
import net.minecraft.server.v1_8_R3.EnchantmentManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class InventoryListener implements Listener {

    public InventoryListener() { }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if(!(e.getWhoClicked() instanceof Player)) {
            return;
        }
        Player p = (Player) e.getWhoClicked();
        if(e.getCurrentItem() == null) {
            return;
        }
        for(ToolGUI toolGUI : ToolGUIManager.getToolGUIS()) {
            if(e.getInventory().getTitle().equalsIgnoreCase(color(toolGUI.getGuiName()))) {
                e.setCancelled(true);
            }
        }
        for(Enchant enchant : EnchantManager.getEnchants()) {
            if(e.getInventory().getTitle().equalsIgnoreCase(color("&d" + enchant.getCategoryName().replace("-", " ")))) {
                e.setCancelled(true);
            }
        }
        if(e.getCurrentItem().hasItemMeta()) {
            if(!e.getCurrentItem().getItemMeta().hasDisplayName()) {
                return;
            }
            for(Enchant enchant : EnchantManager.getEnchants()) {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(color("&d" + enchant.getCategoryName().replaceAll("-", " ")))) {
                    Inventory i = Bukkit.createInventory(null, 27, color("&d" + enchant.getCategoryName().replace("-", " ")));

                    // All enchants to the category
                    for(ItemStack item : enchant.getInvItems()) {
                        i.addItem(item);
                    }

                    // Back items
                    ItemStack back = new ItemStack(Material.BARRIER, 1);
                    ItemMeta backMeta = back.getItemMeta();
                    backMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&cBack"));
                    backMeta.setLore(new ArrayList<>(Arrays.asList(
                            " ",
                            color("&3Click to go back!"),
                            " "
                    )));
                    back.setItemMeta(backMeta);

                    i.setItem(22, back);

                    e.setCancelled(true);
                    p.openInventory(i);
                    return;
                }
            }
            if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(color("&cClose"))) {
                e.setCancelled(true);
                p.closeInventory();
                return;
            }
            if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(color("&cBack"))) {
                e.setCancelled(true);
                ToolGUIManager.openInventory(p);
                return;
            }
            if(e.getCurrentItem().getItemMeta().getLore().get(1) != null) {
                String line = e.getCurrentItem().getItemMeta().getLore().get(1);
                if(line.contains(color("&3Price:"))) {
                    int price = Integer.valueOf(ChatColor.stripColor(line).split(" ")[1].replace("$", " ").trim());
                    if(NPCEnchanter.economy.getBalance(p) < price) {
                        e.setCancelled(true);
                        p.sendMessage(color(FileManager.getErrorCannotAfford()));
                        return;
                    }

                    String displayName = e.getCurrentItem().getItemMeta().getDisplayName();
                    String[] name = ChatColor.stripColor(displayName).split(" - ");
                    Enchantment enchantment = EnchantManager.getAliasEnchant(ChatColor.stripColor(name[0]));

                    if(enchantment == null) {
                        e.setCancelled(true);
                        return;
                    }

                    if(p.getItemInHand().getEnchantmentLevel(enchantment) >= romanToInt(name[1])) {
                        e.setCancelled(true);
                        p.sendMessage(color(FileManager.getPrefix() + FileManager.getErrorItemHasBetterEnch()));
                        p.closeInventory();
                        p.playSound(p.getLocation(), Sound.BAT_DEATH, 10, 1);
                        return;
                    }

                    if(p.getItemInHand().containsEnchantment(Enchantment.DAMAGE_UNDEAD)) {
                        if(enchantment.equals(Enchantment.DAMAGE_ALL)) {
                            e.setCancelled(true);
                            p.sendMessage(color(FileManager.getPrefix() + FileManager.getBadCombo(Objects.requireNonNull(EnchantManager.getEnchant("Sharpness")), Objects.requireNonNull(EnchantManager.getEnchant("Smite")))));
                            p.closeInventory();
                            p.playSound(p.getLocation(), Sound.BAT_DEATH, 10, 1);
                            return;
                        }
                    }
                    if(p.getItemInHand().containsEnchantment(Enchantment.DAMAGE_ALL)) {
                        if(enchantment.equals(Enchantment.DAMAGE_UNDEAD)) {
                            e.setCancelled(true);
                            p.sendMessage(color(FileManager.getPrefix() + FileManager.getBadCombo(Objects.requireNonNull(EnchantManager.getEnchant("Smite")), Objects.requireNonNull(EnchantManager.getEnchant("Sharpness")))));
                            p.closeInventory();
                            p.playSound(p.getLocation(), Sound.BAT_DEATH, 10, 1);
                            return;
                        }
                    }

                    e.setCancelled(true);
                    NPCEnchanter.economy.withdrawPlayer(p, price);
                    EnchantManager.enchant(p.getItemInHand(), enchantment, romanToInt(name[1]));
                    p.sendMessage(color(FileManager.getPrefix() + FileManager.getPurchaseMessage().replace("%enchantment%", ChatColor.stripColor(displayName))
                            .replace("%price%", "" + price)));
                    p.closeInventory();
                    p.playSound(p.getLocation(), Sound.ANVIL_USE, 10, 1);
                }
            }
        }
    }

}
