package me.jackyftw.enchanter.listeners;

import me.jackyftw.enchanter.file.FileManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static me.jackyftw.enchanter.utils.Utils.color;

public class PlayerListener implements Listener {

    public PlayerListener() { }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if(p.isOp()) {
            p.sendMessage(color(FileManager.getPrefix() + " &eThank you for using a plugin created by JackyFTW. "));
        }
    }

}
