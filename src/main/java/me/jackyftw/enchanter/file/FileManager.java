package me.jackyftw.enchanter.file;

import me.jackyftw.enchanter.NPCEnchanter;
import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class FileManager {

    private static File configFile;
    private static FileConfiguration config;

    public static void setup() {
        configFile = new File(NPCEnchanter.instance.getDataFolder(), "config.yml"); // Creates the config file data
        config = NPCEnchanter.instance.getConfig(); // States the config's configuration object

        config.options().copyDefaults(true);
        saveConfig();

        if(!NPCEnchanter.instance.getDataFolder().exists()) {
            NPCEnchanter.instance.getDataFolder().mkdirs(); // Makes plugin folder
        }

        if(!configFile.exists()) {
            try {
                configFile.createNewFile(); // Makes config file
            } catch (IOException e) {
                Utils.log(Level.SEVERE, "Unable to create config.yml!");
            }
        }
    }

    public static FileConfiguration getConfig() {
        return config;
    }

    public static File getConfigFile() {
        return configFile;
    }

    public static void saveConfig() {
        try {
            config.save(configFile);
        } catch (IOException e) {
            Utils.log(Level.SEVERE, "Unable to save config.yml!");
        }
    }

    /*
      Config Options
     */

    public static String getNpcSkin() {
        return config.getString("npc.skin-of");
    }

    public static String getPrefix() {
        return config.getString("messages.chat-prefix");
    }

    public static String getPurchaseMessage() {
        return config.getString("messages.enchant-purchase");
    }

    public static String getErrorHoldingWrongItem() {
        return config.getString("messages.error-holding-wrong-item");
    }

    public static String getErrorCannotAfford() {
        return config.getString("messages.error-cannot-afford");
    }

    public static int getPrice(Enchant enchant, int level) {
        return config.getInt("prices." + enchant.getCategoryName().toLowerCase() + "-" + level);
    }

    public static String getErrorItemHasBetterEnch() {
        return config.getString("messages.error-item-has-better-ench");
    }

    public static String getBadCombo(Enchant ench1, Enchant ench2) {
        return config.getString("messages.error-bad-combo").replace("%ench1%", ench1.getCategoryName()).replace("%ench2%", ench2.getCategoryName());
    }

}
