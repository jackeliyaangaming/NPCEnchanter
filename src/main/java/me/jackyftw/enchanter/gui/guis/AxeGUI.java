package me.jackyftw.enchanter.gui.guis;

import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.gui.ToolGUI;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

import static me.jackyftw.enchanter.utils.Utils.color;

public class AxeGUI extends ToolGUI {

    public AxeGUI() {
        super("&dAxe Enchantments", Material.WOOD_AXE, Material.STONE_AXE, Material.GOLD_AXE,  Material.IRON_AXE, Material.DIAMOND_AXE);
    }

    @Override
    public void compileItems() {
        Inventory inv = getInventory();

        addEnchantCategory(EnchantManager.getEnchant("Efficiency"));
        addEnchantCategory(EnchantManager.getEnchant("Fortune"));
        addEnchantCategory(EnchantManager.getEnchant("Silk-Touch"));
        addEnchantCategory(EnchantManager.getEnchant("Unbreaking"));

        for(Enchant enchant : getEnchantCategories()) {
            String formattedCategoryName = enchant.getCategoryName().replaceAll("-", " ");

            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(color("&d" + formattedCategoryName));
            itemMeta.setLore(new ArrayList<>(Arrays.asList(
                    " ",
                    color("&aClick to view all " + formattedCategoryName + " enchantments!"),
                    " "
            )));
            item.setItemMeta(itemMeta);

            inv.addItem(item);
        }
        setInventory(inv);
    }

}