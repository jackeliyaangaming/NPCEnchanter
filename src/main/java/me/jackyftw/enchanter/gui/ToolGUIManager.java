package me.jackyftw.enchanter.gui;

import me.jackyftw.enchanter.file.FileManager;
import me.jackyftw.enchanter.gui.guis.*;

import static me.jackyftw.enchanter.utils.Utils.color;

import me.jackyftw.enchanter.gui.guis.armor.BootsGUI;
import me.jackyftw.enchanter.gui.guis.armor.ChestplateGUI;
import me.jackyftw.enchanter.gui.guis.armor.HelmetGUI;
import me.jackyftw.enchanter.gui.guis.armor.LeggingsGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ToolGUIManager {

    private static List<ToolGUI> toolGUIS = new ArrayList<>();

    public static List<ToolGUI> getToolGUIS() {
        return toolGUIS;
    }

    public static void registerToolGUIS() {
        toolGUIS.add(new PickaxeGUI());
        toolGUIS.add(new SwordGUI());
        toolGUIS.add(new AxeGUI());
        toolGUIS.add(new ShovelGUI());
        toolGUIS.add(new HelmetGUI());
        toolGUIS.add(new ChestplateGUI());
        toolGUIS.add(new LeggingsGUI());
        toolGUIS.add(new BootsGUI());
        toolGUIS.add(new BowGUI());
        toolGUIS.add(new RodGUI());
    }

    public static ToolGUI getToolGUI(String name) {
        for(ToolGUI toolGUI : getToolGUIS()) {
            if(toolGUI.getGuiName().contains(name)) {
                return toolGUI;
            }
        }
        return null;
    }

    public static void openInventory(Player p) {
        Material materialInHand = p.getItemInHand().getType();
        for(ToolGUI toolGUI : getToolGUIS()) {
            for(Material mat : toolGUI.getMaterials()) {
                if(materialInHand.equals(mat)) {
                    p.openInventory(toolGUI.getInventory());
                    return;
                }
            }
        }

        p.sendMessage(color(FileManager.getPrefix() + FileManager.getErrorHoldingWrongItem()));
    }

}
