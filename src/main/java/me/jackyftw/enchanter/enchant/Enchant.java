package me.jackyftw.enchanter.enchant;

import static me.jackyftw.enchanter.utils.Utils.capitalize;
import static me.jackyftw.enchanter.utils.Utils.color;
import static me.jackyftw.enchanter.utils.Utils.toRoman;

import me.jackyftw.enchanter.file.FileManager;
import me.jackyftw.enchanter.utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class Enchant {

    private String categoryName;
    private Enchantment enchant;
    private List<ItemStack> invItems;

    public Enchant(String categoryName, Enchantment enchant) {
        this.categoryName = categoryName;
        this.enchant = enchant;

        String formattedCategoryName = this.categoryName.replaceAll("-", " ");

        List<ItemStack> items = new ArrayList<>();
        for(int i = 0; i < enchant.getMaxLevel(); i++) {
            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemMeta = item.getItemMeta();
            String romanLevel = toRoman(i + 1);
            itemMeta.setDisplayName(color("&3" + formattedCategoryName + " &7-&3 " + romanLevel));
            ArrayList<String> lore = new ArrayList<>(Arrays.asList(
                    " ",
                    color("&3Price: &d$" + FileManager.getPrice(this, i + 1)),
                    " ",
                    color("&aClick to purchase " + formattedCategoryName + " " + romanLevel + "!"),
                    " "
            ));
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);

            items.add(item);
        }
        this.invItems = items;
    }

    /*
      Getting
     */

    public String getCategoryName() {
        return categoryName;
    }

    public Enchantment getEnchant() {
        return enchant;
    }

    public List<ItemStack> getInvItems() {
        return invItems;
    }

}
