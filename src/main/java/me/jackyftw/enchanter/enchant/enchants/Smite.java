package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Smite extends Enchant {

    public Smite() {
        super("Smite", Enchantment.DAMAGE_UNDEAD);
    }

}
