package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Protection extends Enchant {

    public Protection() {
        super("Protection", Enchantment.PROTECTION_ENVIRONMENTAL);
    }

}
