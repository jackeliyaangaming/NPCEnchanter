package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Power extends Enchant {

    public Power() {
        super("Power", Enchantment.ARROW_DAMAGE);
    }

}
