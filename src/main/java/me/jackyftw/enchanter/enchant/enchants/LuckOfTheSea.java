package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class LuckOfTheSea extends Enchant {

    public LuckOfTheSea() {
        super("Luck-of-the-Sea", Enchantment.LUCK);
    }

}
