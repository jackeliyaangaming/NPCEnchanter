package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class FireProtection extends Enchant {

    public FireProtection() {
        super("Fire-Protection", Enchantment.PROTECTION_FIRE);
    }

}
