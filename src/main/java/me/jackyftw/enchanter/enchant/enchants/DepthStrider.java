package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class DepthStrider extends Enchant {

    public DepthStrider() {
        super("Depth-Strider", Enchantment.DEPTH_STRIDER);
    }

}
