package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class ProjProtection extends Enchant {

    public ProjProtection() {
        super("Projectile-Protection", Enchantment.PROTECTION_PROJECTILE);
    }

}
