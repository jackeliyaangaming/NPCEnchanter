package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class FeatherFalling extends Enchant {

    public FeatherFalling() {
        super("Feather-Falling", Enchantment.PROTECTION_FALL);
    }

}
