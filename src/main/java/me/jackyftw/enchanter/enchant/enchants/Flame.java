package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Flame extends Enchant {

    public Flame() {
        super("Flame", Enchantment.ARROW_FIRE);
    }

}
