package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Efficiency extends Enchant {

    public Efficiency() {
        super("Efficiency", Enchantment.DIG_SPEED);
    }

}
