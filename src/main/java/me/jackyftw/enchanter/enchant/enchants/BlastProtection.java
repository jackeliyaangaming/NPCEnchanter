package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class BlastProtection extends Enchant {

    public BlastProtection() {
        super("Blast-Protection", Enchantment.PROTECTION_EXPLOSIONS);
    }

}
