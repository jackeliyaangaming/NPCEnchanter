package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class BaneOfArthropods extends Enchant {

    public BaneOfArthropods() {
        super("Bane-of-Arthropods", Enchantment.DAMAGE_ARTHROPODS);
    }

}
