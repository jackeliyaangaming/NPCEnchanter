package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Sharpness extends Enchant {

    public Sharpness() {
        super("Sharpness", Enchantment.DAMAGE_ALL);
    }

}
