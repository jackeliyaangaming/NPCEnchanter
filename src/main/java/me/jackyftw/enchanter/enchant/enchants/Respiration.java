package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Respiration extends Enchant {

    public Respiration() {
        super("Respiration", Enchantment.OXYGEN);
    }

}
