package me.jackyftw.enchanter.npc;

import me.jackyftw.enchanter.file.FileManager;
import me.jackyftw.enchanter.utils.Utils;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.skin.SkinnableEntity;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import java.util.logging.Level;

public class NpcManager {

    private static NPC enchanter;
    private static Location enchanterLocation;

    public static NPC getEnchanter() {
        return enchanter;
    }

    /*
      Handler & loading & creating
     */

    public static void registerNpcs() {
        enchanter = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, ChatColor.translateAlternateColorCodes('&', "&dEnchanter"));

        enchanterLocation = new Location(Bukkit.getWorld(FileManager.getConfig().getString("npc.world")), FileManager.getConfig().getDouble("npc.x"),
                FileManager.getConfig().getDouble("npc.y"), FileManager.getConfig().getDouble("npc.z"), (float) FileManager.getConfig().getDouble("npc.yaw"),
                (float) FileManager.getConfig().getDouble("npc.pitch"));
        Utils.log(Level.INFO, "Successfully spawned enchanter [" + enchanterLocation.getWorld().getName() + ", " +
                enchanterLocation.getX() + ", " +
                enchanterLocation.getY() + ", " +
                enchanterLocation.getZ() + "]");
    }

    public static void spawnNpcs() {
        enchanter.spawn(enchanterLocation);
    }

    public static void setSkins() {
        enchanter.data().remove(NPC.PLAYER_SKIN_UUID_METADATA);
        enchanter.data().setPersistent(NPC.PLAYER_SKIN_UUID_METADATA, FileManager.getNpcSkin());
        enchanter.data().setPersistent(NPC.PLAYER_SKIN_USE_LATEST, false);

        if (enchanter.isSpawned()) {
            SkinnableEntity skinnable = enchanter.getEntity() instanceof SkinnableEntity ? (SkinnableEntity) enchanter.getEntity() : null;
            if (skinnable != null) {
                skinnable.setSkinName(FileManager.getNpcSkin(), true);
            }
        }
    }

}
